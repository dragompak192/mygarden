# MyGarden

<p align="center">
  <a href="https://gitlab.com/m9712/mygarden/-/blob/master/LICENSE">
    <img src="https://img.shields.io/badge/license-GPLv3-blue.svg?style=flat-square" alt="MyGarden is released under the GPLv3 license." />
  </a>
  <a href="https://liberapay.com/MyGarden/">
    <img src="https://img.shields.io/liberapay/gives/MyGarden.svg?logo=liberapay&style=flat-square" alt="liberapay MyGarden" />
  </a>
  <a href="https://twitter.com/MyGarden_FR">
    <img src="https://img.shields.io/twitter/follow/MyGarden_FR?style=flat-square&logo=twitter" alt="Follow @MyGarden_FR">
  </a>
  <a href="https://mastodon.social/@MyGarden">
    <img src="https://img.shields.io/mastodon/follow/109252271074531428?logo=mastodon&style=flat-square" alt="Follow @MyGarden_FR">
  </a>
  <a href="https://ionic.link/discord">
    <img src="https://img.shields.io/discord/936209553291370536?color=7289DA&label=%23general&logo=discord&logoColor=white?style=flat-square" alt="Official MyGarden Discord" />
  </a>
  <a href="https://crowdin.com/project/mygardenapp">
    <img src="https://badges.crowdin.net/mygardenapp/localized.svg" alt="Crowdin" />
 </a>
</p>

<a href="https://play.google.com/store/apps/details?id=fr.bugprogEnterprise.mygarden"> 
  <img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png"  width="200">
</a>
<a href="">
  <img src="https://f-droid.org/badge/get-it-on.png"  width="200">
</a>

**A simple app to manage your Garden**
![](./doc/MyGarden_Android_Screenshot.png)

MyGarden is built with Web Technologies:  
[![Typescript](https://img.shields.io/badge/Typescript-3178c6?style=for-the-badge&logo=typescript&labelColor=gray)](https://www.typescriptlang.org/)
[![Angular](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&labelColor=gray)](https://angular.io/)
[![Ionic](https://img.shields.io/badge/IONIC-66b2ff?style=for-the-badge&logo=ionic&labelColor=gray&logoColor=66b2ff)](https://ionicframework.com/)

## How to contribute 🚀

Thank you for your contribution to the open source world !😍

- our forum at [Discord](https://discord.gg/CTVrrCa2YQ) or [Matrix](https://matrix.to/#/#MyGarden:matrix.org)
- for translation on [Crowdin](https://m9712.gitlab.io/vegetables-db/Getting%20started/How_to_translate_MyGarden/)
- add [vegetables](https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/)
- opening issues

## Support

If you need assistance or want to ask a question about the Android app, you are welcome to ask for support in our
Forums.
If you have found a bug, feel free to open a new Issue on Gitlab.
