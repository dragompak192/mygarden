/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import {
  FormControl,
  FormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';
import { VegetablesService } from '../../services/vegetables.service';
import { StorageService } from '../../services/storage.service';
import { TourService } from '../../services/tour.service';
import { Component, Input, OnInit } from '@angular/core';
import { Vegetable } from '../../shared/vegetable.model';
import { Garden } from '../../shared/garden.model';
import { ModalController } from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { ToastNotificationService } from '../../services/toast-notification.service';
import { TranslateService } from '@ngx-translate/core';

export interface VegetableGardenModel {
  id: string;
  name: string;
  isChecked: boolean;
}

@Component({
  selector: 'app-garden-edit',
  templateUrl: './garden-edit.component.html',
  styleUrls: ['./garden-edit.component.scss'],
})
export class GardenEditComponent implements OnInit {
  gardenForm: FormGroup;
  forbiddenGardens = [];
  formIsReady = false;
  allVegetablesAreSelected = false;
  vegetablesForm: VegetableGardenModel[];
  garden: Garden; // Garden data if in edit mode
  @Input() editMode = false;
  @Input() gardenID: string; // If the editMode is true, it contains the name of the editing garden

  constructor(
    private modalCtrl: ModalController,
    private storageService: StorageService,
    private vegetableService: VegetablesService,
    private tourService: TourService,
    private toastNotificationService: ToastNotificationService,
    private translate: TranslateService
  ) {}

  async ngOnInit() {
    // Get gardens names and add them to the blacklist
    this.storageService.getGardens().then((gardens: Garden[]) => {
      this.forbiddenGardens = gardens === null ? [] : gardens;
      this.forbiddenGardens.push({ name: 'firstTime' });
    });
    if (this.editMode) {
      await this.storageService
        .getGarden(this.gardenID)
        .then((garden: Garden) => {
          this.garden = garden;
          this.formInit();
        });
    } else {
      this.formInit();
    }
  }

  forbiddenNames(control: UntypedFormControl): { [s: string]: boolean } {
    // Check if the name is correct or not
    if (this.forbiddenGardens !== undefined && this.forbiddenGardens !== null) {
      if (
        this.forbiddenGardens
          .map((gardenToCheck) => {
            return gardenToCheck.name;
          })
          .indexOf(control.value) !== -1
      ) {
        // Convert Object to array
        return { ['nameIsForbidden']: true }; // Name is forbidden
      } else {
        return null; // Name is allow
      }
    }
    return null;
  }

  onSelectAll() {
    this.allVegetablesAreSelected = !this.allVegetablesAreSelected;
    this.vegetablesForm.map((vegetable: VegetableGardenModel) => {
      return this.allVegetablesAreSelected
        ? (vegetable.isChecked = true)
        : (vegetable.isChecked = false);
    });
  }

  /**
   * Save garden into local storage
   */
  async onSave() {
    // Create list for selected vegetables
    this.gardenForm.value.vegetable = [];
    this.gardenForm.value.extra =
      this.editMode && this.garden.extra ? this.garden.extra : {};
    this.vegetablesForm.forEach((vegetable: VegetableGardenModel) => {
      if (vegetable.isChecked === true) {
        this.gardenForm.value.vegetable.push(vegetable.id);
      }
    });

    this.gardenForm.value.extra.plantingMonth =
      this.vegetableService.getVegetablesPlantingMonths(
        this.gardenForm.value.vegetable
      );

    if (this.gardenForm.valid) {
      if (this.editMode) {
        await this.storageService.updateGarden(
          this.gardenID,
          this.gardenForm.value
        );
      } else {
        this.gardenForm.value.extra.lastDatabaseSchemaVersion =
          environment.lastDatabaseSchemaVersion;
        this.storageService.addGarden(this.gardenForm.value).then(async () => {
          await this.toastNotificationService.presentToast({
            text: this.translate.instant('GLOBAL.gardenSaved'),
          });
        });
      }
    } else {
      console.warn('vegetableForm invalid ! \n', this.gardenForm);
    }
    this.editMode = false;
    this.dismissModal();
    if (this.tourService.isActive()) {
      this.tourService.goTo('startDraw');
    }
  }

  dismissModal() {
    this.editMode = false;
    this.modalCtrl.dismiss();
  }

  private formInit() {
    this.formIsReady = false;
    let name = '';
    let description = '';
    // Vegetables checked uses NgModel
    // Get all Vegetables selected and prepare data to show them in the form
    this.vegetablesForm = this.vegetableService
      .getVegetables()
      .map((vegetable: Vegetable) => {
        return {
          name: vegetable.name,
          id: vegetable.id,
          isChecked:
            this.editMode && this.garden.vegetable.includes(vegetable.id),
        };
      });

    if (this.editMode) {
      // Update form from garden data
      name = this.garden.name;
      description = this.garden.description;

      if (
        this.vegetableService.getVegetablesLen() ===
        this.garden.vegetable.length
      ) {
        this.allVegetablesAreSelected = true;
      }
    }
    this.formIsReady = true;

    // Init the vegetable Form
    this.gardenForm = new FormGroup({
      name: new FormControl(name, [
        Validators.required,
        this.forbiddenNames.bind(this),
        Validators.maxLength(20),
        Validators.minLength(3),
      ]),
      description: new FormControl(description),
    });
  }
}
