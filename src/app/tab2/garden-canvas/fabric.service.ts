/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { DepositModalComponent } from '../../tab1/deposit-modal/deposit-modal.component';
import { VegetablesService } from '../../services/vegetables.service';
import { AlertController, ModalController } from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { StorageService } from '../../services/storage.service';
import { TourService } from '../../services/tour.service';
import { GardenManager } from './garden-manager.service';
import { TranslateService } from '@ngx-translate/core';
import { Garden } from '../../shared/garden.model';
import { Image } from 'fabric/fabric-impl';
import { Injectable } from '@angular/core';
import { fabric } from 'fabric';

interface StrokeCorrectionOption {
  reversedSnap?: boolean;
}

interface ZoneNames {
  name: string;
  group: fabric.Group;
}

@Injectable({ providedIn: 'root' })
export class FabricService {
  canvas: any;
  document: any;
  private defaultScale = 0.1;
  private defaultStrokeWidth = 2;
  private defaultSize = 100;
  private zoneName: ZoneNames[];

  constructor(
    private storageService: StorageService,
    private vegetableService: VegetablesService,
    private translate: TranslateService,
    private gardenManager: GardenManager,
    public alertController: AlertController,
    private modalCtrl: ModalController,
    private tourService: TourService
  ) {}

  initFabric(document: Document) {
    this.document = document;
    const height = document.getElementById('canvasContainer').offsetHeight;
    this.canvas = new fabric.Canvas('canvas', {
      width: window.innerWidth,
      height,
      allowTouchScrolling: true,
      selection: false,
    });

    fabric.Object.prototype.set({
      snapThreshold: 0,
      snapAngle: 18,
      lockScalingFlip: true,
      strokeWidth: 2,
      strokeUniform: true,
      transparentCorners: true,
      cornerColor: '#22A7F0',
      borderColor: '#22A7F0',
      cornerSize: 12,
      originX: 'center',
      originY: 'center',
    });
    this._initControls();
    this.canvas.setDimensions(
      { width: '100%', height: '100%' },
      { cssOnly: true }
    );
    return this.canvas;
  }

  /**
   * Add circle to the view.
   */
  addCircle() {
    const circle = new fabric.Circle({
      width: this.defaultSize,
      top: 0,
      left: 0,
      radius: 75,
      fill: '#D81B60',
      stroke: '"#880E4F',
      strokeWidth: this.defaultStrokeWidth,
      lockRotation: true,
    });
    this._bindImage(circle);
  }

  /**
   * Add rectangle to the view.
   */
  addRect() {
    const rect = new fabric.Rect({
      left: 0,
      top: 0,
      fill: '#D81B60',
      width: this.defaultSize,
      height: this.defaultSize,
      objectCaching: false,
      stroke: '"#880E4F',
      strokeWidth: this.defaultStrokeWidth,
      centeredRotation: true,
    });
    this._bindImage(rect);
  }

  /**
   * Add triangle.
   */
  addTriangle() {
    const triangle = new fabric.Triangle({
      left: 0,
      top: 0,
      fill: '#D81B60',
      width: this.defaultSize,
      height: this.defaultSize,
      objectCaching: false,
      stroke: '"#880E4F',
      strokeWidth: this.defaultStrokeWidth,
    });
    this._bindImage(triangle);
  }

  /**
   * Add text to the view.
   */
  addText() {
    const txt = new fabric.IText(this.translate.instant('TAB2.ItextEdit'), {
      left: 200,
      top: 100,
      fill: 'black',
      fontFamily: 'Delicious_500',
      selectable: true,
      lockScalingFlip: true,
      fontSize: 25,
    });
    this.canvas.add(txt);
    this.canvas.setActiveObject(txt);
    this.canvas.requestRenderAll();
  }

  /**
   * Unlock the garden map.
   */
  unlockEdit() {
    this.canvas.selection = true;
    const objects = this.canvas.getObjects();
    for (const item of objects) {
      item.set({
        hasControls: true,
        selectable: true,
      });
    }
  }

  /**
   * Discard active object and set selection to false.
   */
  lockEdit() {
    this.canvas.selection = false;
    this.canvas.discardActiveObject();
    const objects = this.canvas.getObjects();
    for (const item of objects) {
      item.set({
        hasControls: false,
        selectable: false,
      });
    }
    this.canvas.requestRenderAll();
  }

  /**
   * Save garden into local storage.
   * @param gardenName The garden name
   * @param year The year to save
   * @param month the month to save
   */
  saveGardenDraw(gardenName: string, year: number, month: number) {
    const gardenJSON = JSON.stringify(
      this.canvas.toJSON(['name', 'vegetableId'])
    );
    return this.storageService.updateMap(gardenName, gardenJSON, year, month);
  }

  /**
   * Restore the garden map form JSON stored in the local storage then lock edit.
   * @param gardenName The garden ID
   * @param year The year to restore
   * @param month The month to restore
   */
  restoreFromJSON(gardenName: string, year: number, month: number) {
    if (!this.canvas) {
      console.log("Can't restore JSON, canvas is undefined");
      return;
    }
    this.canvas.clear(); // Reset canvas
    this.zoneName = [];
    if (gardenName === null || !year || month === null) {
      return;
    }
    this.storageService.getGarden(gardenName).then((garden: Garden) => {
      if (Object.keys(garden.maps).length !== 0 && garden.maps[year] !== null) {
        this.canvas.loadFromJSON(garden.maps[year][month], () => {
          this.lockEdit();
          this.initObjects(); // Restore features
          // Restore zoneName
          for (const targetObj of this.canvas.getObjects('group')) {
            this.zoneName.push({ name: targetObj.name, group: targetObj });
          }
        });
      }
    });
  }

  /**
   * Assign for each zone a vegetable.
   * @param gardenName the name of the garden.
   */
  vegetableAssignment(gardenName: string): Promise<number> {
    return new Promise((resolve, reject): void => {
      const error = setTimeout(() => {
        return reject(2);
      }, 15000);

      this.storageService.getGarden(gardenName).then(async (garden: Garden) => {
        // Call tour dialog
        if (this.tourService.isActive()) {
          this.tourService.goTo('yearSelect');
        }

        // Do some checks
        if (
          garden.vegetable.length <= 0 ||
          this.canvas.getObjects('group').length <= 0
        ) {
          return reject(3);
        }

        // STEP 1: Prepare data.
        const timelineInterval: {
          start: number;
          end: number;
          interval: () => number;
        } = {
          start: new Date().getFullYear(),
          end: Math.max.apply(null, Object.keys(garden.maps)),
          interval() {
            return this.end - this.start;
          },
        };
        // STEP 2: Generate maps.
        this.gardenManager
          .runEvolution(
            this.canvas.getObjects('group'),
            garden.vegetable,
            this.zoneName.map((x) => x.name),
            garden.extra.plantingMonth,
            timelineInterval
          )
          .then(([vegetablePlacement, status]) => {
            console.log('Vegetables Placement: ', vegetablePlacement);

            // STEP 3: Update images.
            const getVegetableFromZone = (
              year: string,
              month: string,
              zone: string
            ): string => {
              return (
                (
                  vegetablePlacement[year][month].find(
                    (x) => x.zoneName === zone
                  ) || {}
                ).vegetableId || null
              );
            };

            const fabricTemplate =
              garden.maps[timelineInterval.start][
                Math.min(...garden.extra.plantingMonth)
              ];

            for (const year of Object.keys(garden.maps)) {
              garden.maps[year] = {};
              for (const month of Object.keys(vegetablePlacement[year])) {
                // Copy fabricTemplate to all month objects
                garden.maps[year][month] = JSON.parse(
                  JSON.stringify(fabricTemplate)
                ); // TODO: temporary workaround

                for (const object of garden.maps[year][month].objects) {
                  const zoneName = object.name;
                  const vegetableId = getVegetableFromZone(
                    year,
                    month,
                    zoneName
                  );
                  if (vegetableId === null) {
                    // Clear the zone
                    object.vegetableId = null;
                    object.objects[1].src = null;
                    continue;
                  }
                  object.vegetableId = vegetableId;
                  object.objects[1].src =
                    this.vegetableService.getVegetableById(
                      vegetableId
                    ).imagePath;
                }
              }
            }
            // STEP 4: Save changes and reload UI.
            this.storageService
              .updateMaps(gardenName, JSON.stringify(garden.maps))
              .then(() => {
                this.storageService.gardenSelected.next({
                  garden: garden,
                  date: new Date(
                    new Date().getFullYear(),
                    Math.min(...garden.extra.plantingMonth)
                  ),
                });
              });
            clearTimeout(error);
            return resolve(status);
          });
      });
    });
  }

  /**
   * Allow to delete an element and init snap to edge feature.
   * @private
   */
  private initObjects() {
    this._initControls();
    this.canvas.on('object:moving', (object: any) => {
      this.snapToEdge(object, 'moving');
    });
    this.canvas.on('object:scaling', (object: any) => {
      this.snapToEdge(object, 'scaling');
    });
    this.canvas.on('selection:cleared', () => {
      this._updateScale();
    });
    const objects = this.canvas.getObjects();
    for (const item of objects) {
      item.on('rotating', () => {
        this._updateRotation(item);
      });
      item.on('mousedown', async (e) => {
        if (
          !this.canvas.selection &&
          e.target !== null &&
          e.target.vegetableId
        ) {
          const modal = await this.modalCtrl.create({
            component: DepositModalComponent,
            componentProps: {
              vegetable: this.vegetableService.getVegetableById(
                e.target.vegetableId
              ),
            },
          });
          await modal.present();
        }
      });
    }
  }

  /**
   * Generate a single letter for a zone [A-Z].
   * @private
   * @return Single capital letter.
   */
  private generateName(): string {
    if (this.zoneName.length >= 26) {
      console.error('Limit of 26 reached !');
      return;
    }
    const alphabet = [...'ABCDEFGHIJKLMNOPQRSTUVWXZ'];
    let i = 0;
    while (this.zoneName.map((x) => x.name).includes(alphabet[i])) {
      i += 1;
    }
    return alphabet[i];
  }

  /**
   * Snap the objects to each other.
   * @param activeObject The object to currently used.
   * @param mode Mode used to snap the object
   * @private
   */
  private snapToEdge(activeObject: any, mode: 'moving' | 'scaling') {
    const snapZone = 10;

    const activeObjectHeight =
      activeObject.target.height * activeObject.target.scaleY;
    const activeObjectBottom =
      activeObject.target.top +
      activeObject.target.height * activeObject.target.scaleY;
    const activeObjectWidth =
      activeObject.target.width * activeObject.target.scaleX;
    const activeObjectRight =
      activeObject.target.left +
      activeObject.target.width * activeObject.target.scaleX;
    const activeObjectLeft = activeObject.target.left;
    const activeObjectTop = activeObject.target.top;
    const activeObjectScaleX = activeObject.target.scaleX;
    const activeObjectScaleY = activeObject.target.scaleY;

    const defaultSize = this.defaultSize + this.defaultStrokeWidth;

    for (const targetObj of this.canvas.getObjects('group')) {
      const isSameObject = targetObj === activeObject.target;

      const targetObjRight =
        targetObj.left + targetObj.width * targetObj.scaleX;
      const targetObjLeft = targetObj.left;
      const targetObjBottom =
        targetObj.top + targetObj.height * targetObj.scaleY;
      const targetObjTop = targetObj.top;
      const targetObjectScaleX = targetObj.scaleX;
      const targetObjectScaleY = targetObj.scaleY;

      const strokeCorrectionX = (options?: StrokeCorrectionOption): number => {
        let correction =
          options && options.reversedSnap ? 0 : this.defaultStrokeWidth;
        // Apply correction for the activeObject
        correction =
          0.5 * (correction + this.defaultStrokeWidth * activeObjectScaleX);
        if (options && options.reversedSnap) {
          correction -= this.defaultStrokeWidth * 0.5;
        }
        // Apply correction for targetObject
        const i = options && options.reversedSnap ? -1 : 1;
        correction =
          correction +
          i *
            0.5 *
            (this.defaultStrokeWidth * targetObjectScaleX -
              this.defaultStrokeWidth);
        return correction;
      };

      const strokeCorrectionY = (options?: StrokeCorrectionOption): number => {
        let correction =
          options && options.reversedSnap ? 0 : this.defaultStrokeWidth;
        // Apply correction for the activeObject
        correction =
          0.5 * (correction + this.defaultStrokeWidth * activeObjectScaleY);
        if (options && options.reversedSnap) {
          correction = correction * -1 + this.defaultStrokeWidth * 0.5;
        }
        // Apply correction for targetObject
        correction =
          correction +
          0.5 *
            (this.defaultStrokeWidth * targetObjectScaleY -
              this.defaultStrokeWidth);
        return correction;
      };

      // Snap right active object to the left object target
      if (
        activeObjectRight > targetObjLeft - snapZone &&
        activeObjectRight < targetObjLeft + snapZone &&
        !isSameObject
      ) {
        if (mode === 'moving') {
          activeObject.target
            .set({
              left: targetObjLeft - activeObjectWidth + strokeCorrectionX(),
            })
            .setCoords();
        } else if (mode === 'scaling') {
          activeObject.target
            .set({
              scaleX:
                (targetObjLeft - activeObjectLeft + strokeCorrectionX()) /
                defaultSize,
            })
            .setCoords();
        }
      }

      // Snap active object left to the right object target
      if (
        activeObjectLeft > targetObjRight - snapZone &&
        activeObjectLeft < targetObjRight + snapZone &&
        !isSameObject
      ) {
        if (mode === 'moving') {
          activeObject.target
            .set({
              left: targetObjRight - strokeCorrectionX(),
            })
            .setCoords();
        }
      }

      // Snap active object top to the bottom object target
      if (
        activeObjectTop > targetObjBottom - snapZone &&
        activeObjectTop < targetObjBottom + snapZone &&
        !isSameObject
      ) {
        if (mode === 'moving') {
          activeObject.target
            .set({
              top: targetObjBottom - strokeCorrectionY(),
            })
            .setCoords();
        }
      }

      // Snap active object bottom to the top object target
      if (
        activeObjectBottom > targetObjTop - snapZone &&
        activeObjectBottom < targetObjTop + snapZone
      ) {
        if (mode === 'moving') {
          activeObject.target
            .set({
              top: targetObjTop - activeObjectHeight + strokeCorrectionY(),
            })
            .setCoords();
        } else if (mode === 'scaling') {
          activeObject.target
            .set({
              scaleY:
                (targetObjTop - activeObjectTop + strokeCorrectionY()) /
                defaultSize,
            })
            .setCoords();
        }
      }

      // Snap active object top angles to the top object target angles
      if (
        activeObjectTop > targetObjTop - snapZone &&
        activeObjectTop < targetObjTop + snapZone &&
        !isSameObject
      ) {
        if (mode === 'moving') {
          activeObject.target
            .set({
              top: targetObjTop + strokeCorrectionY({ reversedSnap: true }),
            })
            .setCoords();
        }
      }

      // Snap active object bottom angles to the bottom object target angles
      if (
        activeObjectBottom > targetObjBottom - snapZone &&
        activeObjectBottom < targetObjBottom + snapZone &&
        !isSameObject
      ) {
        if (mode === 'moving') {
          activeObject.target
            .set({
              top:
                targetObjBottom -
                activeObjectHeight -
                strokeCorrectionY({ reversedSnap: true }),
            })
            .setCoords();
        } else if (mode === 'scaling') {
          activeObject.target
            .set({
              scaleY:
                (targetObjBottom -
                  activeObjectTop -
                  strokeCorrectionY({ reversedSnap: true })) /
                defaultSize,
            })
            .setCoords();
        }
      }

      // Snap active object left angles to the left object target angles
      if (
        activeObjectLeft > targetObjLeft - snapZone &&
        activeObjectLeft < targetObjLeft + snapZone
      ) {
        if (mode === 'moving') {
          activeObject.target
            .set({
              left: targetObjLeft - strokeCorrectionX({ reversedSnap: true }),
            })
            .setCoords();
        }
      }

      // Snap active object right angles to the right object target angles
      if (
        activeObjectRight > targetObjRight - snapZone &&
        activeObjectRight < targetObjRight + snapZone &&
        !isSameObject
      ) {
        if (mode === 'moving') {
          activeObject.target
            .set({
              left:
                targetObjRight -
                activeObjectWidth +
                strokeCorrectionX({ reversedSnap: true }),
            })
            .setCoords();
        } else if (mode === 'scaling') {
          activeObject.target
            .set({
              scaleX:
                (targetObjRight -
                  activeObjectLeft +
                  strokeCorrectionX({ reversedSnap: true })) /
                defaultSize,
            })
            .setCoords();
        }
      }
    }
  }

  /**
   * Add objects controls
   * @private
   */
  private _initControls() {
    const deleteIcon: string =
      "data:image/svg+xml,%3C%3Fxml version='1.0' encoding='utf-8'%3F%3E%3C!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'%3E%3Csvg version='1.1' id='Ebene_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='595.275px' height='595.275px' viewBox='200 215 230 470' xml:space='preserve'%3E%3Ccircle style='fill:%23F44336;' cx='299.76' cy='439.067' r='218.516'/%3E%3Cg%3E%3Crect x='267.162' y='307.978' transform='matrix(0.7071 -0.7071 0.7071 0.7071 -222.6202 340.6915)' style='fill:white;' width='65.545' height='262.18'/%3E%3Crect x='266.988' y='308.153' transform='matrix(0.7071 0.7071 -0.7071 0.7071 398.3889 -83.3116)' style='fill:white;' width='65.544' height='262.179'/%3E%3C/g%3E%3C/svg%3E";
    const img = document.createElement('img');
    img.src = deleteIcon;
    fabric.Object.prototype.controls.deleteControl = new fabric.Control({
      x: 0,
      y: -0.5,
      offsetY: -30,
      offsetX: 40,
      cursorStyle: 'pointer',
      mouseUpHandler: (): boolean => {
        const activeObjects = this.canvas.getActiveObjects();
        if (activeObjects.length > 0) {
          for (const activeObject of activeObjects) {
            this.zoneName = this.zoneName.filter(
              (value) => activeObject.name !== value
            );
            this.canvas.remove(activeObject);
          }
          this.canvas.discardActiveObject();
          this.canvas.renderAll();
        }
        return true;
      },
      render: (ctx, left, top, styleOverride, fabricObject) => {
        const size = 24;
        ctx.save();
        ctx.translate(left, top);
        ctx.rotate(fabric.util.degreesToRadians(fabricObject.angle));
        ctx.drawImage(img, -size / 2, -size / 2, size, size);
        ctx.restore();
      },
    });
  }

  /**
   * Update the img scale on resize.
   * @private
   */
  private _updateScale() {
    const objects = this.canvas.getObjects('group'); // Get objets and exclude iText object
    for (const object of objects) {
      if (object.scaleX < 1) {
        // True when the width is too small compared to the height
        object.getObjects()[1].scale(0.2);
        object.getObjects()[1].scaleY = (object.scaleX / object.scaleY) * 0.2;
      }
      // True when the height is too small compared to the width
      else if (object.scaleY < 1) {
        object.getObjects()[1].scale(0.2);
        object.getObjects()[1].scaleX = (object.scaleY / object.scaleX) * 0.2;
      } else {
        object.getObjects()[1].scaleX =
          (object.scaleY / object.scaleX) * this.defaultScale;
        object.getObjects()[1].scaleY = this.defaultScale; // Reset Y; in case if the object matched the condition: 1
      }
    }
  }

  private _updateRotation(group: fabric.Group) {
    // Need support !
    const newAngle = -group.angle;
    if (group.angle === 0 || group.angle === 180) {
      group.getObjects()[1].rotate(newAngle);
    }
  }

  /**
   * Bind the vegetable image into to object (Rect, Triangle, Circle).
   * @param object the object to bind with the image
   * @private
   */
  private _bindImage(object: fabric.Rect | fabric.Circle | fabric.Triangle) {
    fabric.Image.fromURL(null, (img: Image) => {
      const img1 = img.scale(this.defaultScale).set({ left: 0, top: 0 });
      const group = new fabric.Group([object, img1], {
        left: 20,
        top: 20,
        centeredRotation: true,
        originX: 'left',
        originY: 'top',
        name: this.generateName(),
      });
      this.canvas.add(group);
      this.zoneName.push({ name: group.name, group: null });
      group.sendToBack();
      if (!environment.production) {
        // const txt = new fabric.Text(group.name); // Only for debug, write the zone name.
        // group.add(txt);
      }
      this.canvas.setActiveObject(group).requestRenderAll();
      this.initObjects(); // Add group features
    });
  }
}
