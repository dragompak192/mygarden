/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Tab1PageRoutingModule } from './tab1-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { VegetableItemComponent } from './vegetables-list/vegetable-item/vegetable-item.component';
import { VegetablesListComponent } from './vegetables-list/vegetables-list.component';
import { InGardenComponent } from './deposit-modal/in-garden/in-garden.component';
import { DepositModalComponent } from './deposit-modal/deposit-modal.component';
import { GeneralComponent } from './deposit-modal/ID-card/general.component';
import { AboutComponent } from './deposit-modal/about/about.component';
import { Tab1Component } from './tab1.component';

import { ShortenPipe } from './shorten.pipe';

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    CommonModule,
    TranslateModule,
    Tab1PageRoutingModule,
  ],
  declarations: [
    Tab1Component,
    ShortenPipe,
    AboutComponent,
    GeneralComponent,
    InGardenComponent,
    DepositModalComponent,
    VegetableItemComponent,
    VegetablesListComponent,
  ],
})
export class Tab1PageModule {}
