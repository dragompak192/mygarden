/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { DepositModalComponent } from '../deposit-modal/deposit-modal.component';
import { VegetablesService } from '../../services/vegetables.service';
import { LanguageService } from '../../services/language.service';
import { Vegetable } from '../../shared/vegetable.model';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-vegetables-list',
  templateUrl: './vegetables-list.component.html',
  styleUrls: ['./vegetables-list.component.scss'],
})
export class VegetablesListComponent implements OnInit {
  seasonalVegetables: Vegetable[] = [];
  vegetableView = 'ascending';
  vegetables: Vegetable[];
  vegetablesFiltered: Vegetable[];
  filtersSelected = ['fruit', 'vegetable'];

  constructor(
    private modalCtrl: ModalController,
    public vegetablesServices: VegetablesService,
    private languageService: LanguageService
  ) {}

  ngOnInit() {
    this.loadVegetable();

    this.languageService.languageUpdated.subscribe(() => {
      this.loadVegetable();
    });

    for (const vegetableItem of this.vegetables) {
      let currentMonth = new Date().getMonth() + 1; // Get the month as a number (1-12)

      // Check if vegetableItem.cultureSheets.monthOfHarvest is of type number or number[]
      const earlySeason = Array.isArray(
        vegetableItem.cultureSheets.monthOfHarvest
      )
        ? vegetableItem.cultureSheets.monthOfHarvest[0]
        : vegetableItem.cultureSheets.monthOfHarvest;
      let endSeason = Array.isArray(vegetableItem.cultureSheets.monthOfHarvest)
        ? vegetableItem.cultureSheets.monthOfHarvest[1] + 1
        : vegetableItem.cultureSheets.monthOfHarvest + 2;

      // Add 12 months to "endSeason" if the interval is reverse (EX: JANUARY - APRIL -> APRIL - JANUARY)
      // Add 12 months to "currentMonth" to adjust it if necessary
      // tslint:disable-next-line:no-unused-expression
      endSeason < earlySeason &&
        ((endSeason += 12), currentMonth < earlySeason && (currentMonth += 12));

      if (currentMonth >= earlySeason && currentMonth <= endSeason) {
        const vegetable: Vegetable = this.vegetablesServices.getVegetableByName(
          vegetableItem.name
        );
        this.seasonalVegetables.push(vegetable);
      }
    }
  }

  loadVegetable() {
    this.vegetables = this.vegetablesServices.getVegetables();
    this.vegetablesFiltered = this.vegetables.filter((x) =>
      this.filtersSelected.includes(x.type)
    );
    this.vegetablesFiltered = this.vegetablesServices.sort(
      this.vegetablesFiltered,
      this.vegetableView
    );
  }

  async onVegetableSelected(vegetable: Vegetable) {
    // Send data to the deposit-modal component
    const modal = await this.modalCtrl.create({
      component: DepositModalComponent,
      componentProps: {
        vegetable,
      },
    });
    await modal.present();
  }

  onUpdateButton() {
    if (this.vegetableView === 'descending') {
      this.vegetableView = 'ascending';
    } else {
      this.vegetableView = 'descending';
    }
    this.vegetablesFiltered = this.vegetablesServices.sort(
      this.vegetablesFiltered,
      this.vegetableView
    );
  }

  onSwitch(filter: string) {
    if (this.filtersSelected.includes(filter)) {
      if (this.filtersSelected.length <= 1) {
        return;
      }
      this.filtersSelected = this.filtersSelected.filter((value) => {
        return value !== filter;
      });
    } else {
      this.filtersSelected.push(filter);
    }
    this.vegetablesFiltered = this.vegetables.filter((x) =>
      this.filtersSelected.includes(x.type)
    );
  }
}
