/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Component, Input, OnInit } from '@angular/core';
import { Vegetable } from '../../../shared/vegetable.model';
import { VegetablesService } from '../../../services/vegetables.service';

@Component({
  selector: 'app-in-garden',
  templateUrl: './in-garden.component.html',
  styleUrls: ['./in-garden.component.scss'],
})
export class InGardenComponent implements OnInit {
  @Input() vegetable: Vegetable;
  monthOfHarvest = [];
  monthOfPlanting = [];

  constructor(private vegetableService: VegetablesService) {}

  ngOnInit() {
    this.monthOfHarvest = this.vegetableService.convertMonthToString(
      this.vegetable.cultureSheets.monthOfHarvest
    );
    this.monthOfPlanting = this.vegetableService.convertMonthToString(
      this.vegetable.cultureSheets.monthOfPlanting
    );
  }
}
