/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

export interface ToastButton {
  text: string;
  icon?: string;
  side?: 'start' | 'end';
  role?: 'cancel' | string;
  cssClass?: string | string[];
  handler?: () => boolean | void | Promise<boolean | void>;
}

@Injectable({ providedIn: 'root' })
/**
 * Toast notification service
 * @link https://ionicframework.com/docs/api/toast
 */
export class ToastNotificationService {
  constructor(public toastController: ToastController) {}

  /**
   * Show a toast notification
   * @param data Object
   */
  async presentToast(data: ToastButton) {
    const toast = await this.toastController.create({
      message: data.text,
      duration: 2000,
    });
    toast.present();
  }
}
