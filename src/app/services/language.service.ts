/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from './storage.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LanguageService {
  languageUpdated = new Subject<string>();

  constructor(
    private translate: TranslateService,
    private storageService: StorageService
  ) {}

  /**
   * Initialize languages at application startup.
   */
  setInitialAppLanguage() {
    this.storageService.getLanguageSelected().then((languageSelected) => {
      const language =
        languageSelected === null
          ? this.translate.getBrowserLang()
          : languageSelected;
      console.log(
        'Selected language:',
        languageSelected,
        '| Browser language :',
        this.translate.getBrowserLang()
      );
      this.translate.setDefaultLang('en');
      this.setLanguage(language);
      this.languageUpdated.next(language);
    });
  }

  getLanguage() {
    return this.translate.currentLang;
  }

  /**
   * Change app language.
   * @param lng the new language.
   */
  setLanguage(lng: string) {
    this.storageService.updateLanguage(lng).then(() => {
      this.translate.use(String(lng));
      this.languageUpdated.next(lng);
    });
  }
}
