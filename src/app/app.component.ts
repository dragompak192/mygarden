/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import {
  ThemeDetection,
  ThemeDetectionResponse,
} from '@awesome-cordova-plugins/theme-detection/ngx';
import { ScreenOrientation } from '@awesome-cordova-plugins/screen-orientation/ngx';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { LanguageService } from './services/language.service';
import { MigrationService } from './services/migration.service';
import { isPlatform, ModalController } from '@ionic/angular';
import { HistoryService } from './services/history.service';
import { LottiePlayer } from '@lottiefiles/lottie-player';
import { StatusBar, Style } from '@capacitor/status-bar';
import { TourService } from './services/tour.service';
import { Router } from '@angular/router';
import { App } from '@capacitor/app';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  lottieAnimationSubscription: Subscription;
  lottieEle;

  constructor(
    private languageService: LanguageService,
    private historyService: HistoryService,
    private router: Router,
    private themeDetection: ThemeDetection,
    private modalCtrl: ModalController,
    private screenOrientation: ScreenOrientation,
    private tourService: TourService,
    private migrationService: MigrationService
  ) {}

  ngOnInit() {
    if (isPlatform('mobile')) {
      this.backButtonInit();
      this.themeInit();
      // TODO: Temporary workaround to avoid issues.
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    this.languageService.setInitialAppLanguage();

    this.lottieAnimationSubscription =
      this.tourService.lottieAnimation.subscribe((e: boolean) => {
        if (e) {
          this.lottieEle.style.zIndex = 999;
          this.lottieEle.play();
          this.lottieEle.addEventListener('complete', () => {
            this.lottieEle.style.zIndex = -999;
            this.lottieEle.destroy();
          });
        }
      });

    this.migrationService.isUpToDate().then((res: boolean) => {
      console.log(res);
      if (res) {
        this.migrationService.updateDatabaseSchema();
      }
    });
  }

  backButtonInit() {
    App.addListener('backButton', () => {
      if (this.tourService.isActive()) {
        return;
      } // Disable backButton if the user tour is active.
      this.modalCtrl.getTop().then((modal) => {
        if (modal) {
          this.modalCtrl.dismiss(); // close modal if opened
        } else {
          const precedentTab = this.historyService.pop();
          if (precedentTab) {
            this.router.navigate(['tabs', this.historyService.currentTab]);
          } else {
            App.minimizeApp(); // or minimize app
          }
        }
      });
    });
  }

  themeInit() {
    const setStatusBarStyleDark = async () => {
      await StatusBar.setStyle({ style: Style.Dark });
    };
    const setStatusBarStyleLight = async () => {
      await StatusBar.setStyle({ style: Style.Light });
      await StatusBar.setBackgroundColor({ color: '#ffffff' });
    };

    this.themeDetection
      .isAvailable()
      .then((res: ThemeDetectionResponse) => {
        if (res.value) {
          this.themeDetection
            .isDarkModeEnabled()
            .then((response: ThemeDetectionResponse) => {
              if (response.value) {
                setStatusBarStyleDark().then();
              } else {
                setStatusBarStyleLight().then();
              }
            })
            .catch((error: any) => console.error(error));
        }
      })
      .catch((error: any) => console.error(error));
  }

  ngAfterViewInit() {
    this.lottieEle = document.querySelector('lottie-player') as LottiePlayer;
  }

  ngOnDestroy() {
    this.lottieAnimationSubscription.unsubscribe();
  }
}
