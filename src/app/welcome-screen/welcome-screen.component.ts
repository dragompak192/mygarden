/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import SwiperCore, { Pagination, SwiperOptions } from 'swiper';
import { LanguageService } from '../services/language.service';
import { StorageService } from '../services/storage.service';
import { TourService } from '../services/tour.service';
import { SwiperComponent } from 'swiper/angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

SwiperCore.use([Pagination]);

@Component({
  selector: 'app-welcome-screen',
  templateUrl: 'welcome-screen.component.html',
  styleUrls: ['welcome-screen.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class WelcomeScreenComponent implements OnInit, OnDestroy {
  @ViewChild('swiper') swiper: SwiperComponent;
  selectedLanguage = 'en';
  selectedLanguageSub: Subscription;
  isDarkModeEnable = window.matchMedia('(prefers-color-scheme: dark)').matches; // Use matchMedia to check the user preference

  config: SwiperOptions = {
    pagination: true,
  };

  constructor(
    private storageService: StorageService,
    private router: Router,
    private languageService: LanguageService,
    private tourService: TourService
  ) {}

  ngOnInit() {
    this.selectedLanguageSub = this.storageService.languageSelected.subscribe(
      (lang: string) => {
        this.selectedLanguage = lang;
      }
    );
  }

  start() {
    this.storageService.unLockApp().then(() => {
      this.router.navigate(['/tabs', 'tab1']);
      this.tourService.start();
    });
  }

  onSwitchLanguage(language) {
    this.languageService.setLanguage(language.detail.value);
  }

  ngOnDestroy() {
    this.selectedLanguageSub.unsubscribe();
  }
}
