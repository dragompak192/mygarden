/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

/**
 * Vegetable model.
 * Documentation: https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/
 * @author DERACHE Adrien a.d44@tuta.io
 */
export class Vegetable {
  public id: string;
  public name: string;
  public type: 'fruit' | 'vegetable' | 'plant';
  public fromTree?: boolean;
  public family: string;
  public synopsis: string;
  public preservation: string | null;
  public nutritional: string | null;
  public vitamin: string | null;
  public history: string;
  public imagePath: string;
  public cultureSheets: {
    monthOfPlanting: number | number[];
    monthOfHarvest: number | number[];
    plantingInterval?: number | null;
    plantation: string;
    harvest?: string | null;
    soil?: string | null;
    tasks?: string | null;
    favourableAssociation: string[] | null;
    unfavourableAssociation: string[] | null;
    favourablePrecedents: string[] | null;
    unfavourablePrecedents: string[] | null;
  };

  constructor(
    id: string,
    name: string,
    type: 'fruit' | 'vegetable' | 'plant',
    fromTree = false,
    family: string,
    synopsis: string,
    preservation: string | null,
    nutritional: string | null,
    vitamin: string | null,
    history: string,
    imagePath: string,
    cultureSheets: {
      monthOfPlanting: number[] | number;
      monthOfHarvest: number[] | number;
      plantingInterval?: number | null;
      plantation: string;
      harvest?: string | null;
      soil?: string | null;
      tasks?: string | null;
      favourableAssociation: string[] | null;
      unfavourableAssociation: string[] | null;
      favourablePrecedents: string[] | null;
      unfavourablePrecedents: string[] | null;
    }
  ) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.fromTree = fromTree;
    this.family = family;
    this.synopsis = synopsis;
    this.preservation = preservation;
    this.nutritional = nutritional;
    this.vitamin = vitamin;
    this.history = history;
    this.imagePath = imagePath;
    this.cultureSheets = cultureSheets;
  }
}
