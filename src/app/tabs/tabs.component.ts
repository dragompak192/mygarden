/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { HistoryService } from '../services/history.service';
import { Component, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { TourService } from '../services/tour.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.component.html',
  styleUrls: ['tabs.component.scss'],
})
export class TabsComponent {
  @ViewChild('myTabs', { static: false }) myTabs: IonTabs;

  constructor(
    private historyService: HistoryService,
    private tourService: TourService
  ) {}

  clickTab(tab: string) {
    if (this.tourService.isActive()) {
      this.tourService.goTo('addGardenButton');
    }
    this.historyService.updateTabSelected(tab);
    this.historyService.push(tab);
  }
}
